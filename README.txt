Description
------------------

Drupal has a very powerful image manipulation UI, so why write code when we can easily use that for any purpose?!
The problem is we can't! The reason is image manipulation UI only accepts one argument which is the source image.
All other parameters and options are static. For example if you want to put the source image behind a frame you can,
but if you want to dynamically change frame's image you either have to make new preset/style for each different frame
or write a custom special action.

Another example is rendering texts dynamically, you can pass any text you want to the image style and have it rendered without writing any custom action

This small module adresses this problem by letting you pass any argument to image module, parameters pass via query string
and you using them you can override any style parameters on the fly.

All you have to do is to use image_style_argument_url instead of image_style_url when generating the image url

Note that is module is not compatible with drupal image caching system out of the box

INSTALLATION
------------

Just install and enable the module



Usage
------------------

Use the following code to see the sytle's parameters
  $style = image_style_load('style_name');
  print_r($style);
  
Use the following code override style parameters and generate image url (effects index numbers are actually their ids , so they will not change when you add/remove effects or even when you rearrange them) : 
  
  $style_override = array('effects' => array(
    9 => array ( //Overlay (watermark)
	  'data' => array (
	    'path' => 'public://my_frame.jpg'
	  )
	)
  ));
  image_style_argument_url('style_name', 'public://myimage.jpg', $style_override);
  
You can also use theme_image_style_argument() as an alternative to theme_image_style()
  theme_image_style_argument(array('data') => $style_override)
  
By default it does not support image caching since Drupal bypasses caching whenever url has query string, to fix this problem and also shortening the url length you can define 
your own special logic simply by implementing two hooks.
- mymodule_image_style_argument_url_override(&$uri, &$url, &$style_name, &$path, &$data)
- mymodule_image_style_argument_override(&$image_uri, &$derivative_uri, &$style, &$scheme)


  
Future
------------------

This module is just a temporary workaround , the ideal solution is proper argument support by drupal's core image module , something like Views arguments
Also it's not currently  possible to use tokens or context in image styles/presets. One of the advantages of token support is to use parameters of the last effect 
in the next one. For example we can scale an image by width and then use the new height size to crop image's width and height in the next effect. This also opens up the possibility
to have much smarter effects and combine them together like Color detector, it can detect the major colors used in an image and when can use the most used one as the image's background color or color filter!

This module also introduces two new hooks, if they're ported to Drupal image module , third party modules can extend it to support arguments
- image_style_argument_url_override
- image_style_argument_override